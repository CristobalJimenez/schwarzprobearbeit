<?php


namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ImprintController extends Controller
{

    /**
     * @Route("/imprint", name="imprint")
     */
    public function imprintAction(Request $request)
    {
        return $this->render('default/imprint.html.twig');
    }
}