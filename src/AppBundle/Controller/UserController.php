<?php


namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use GuzzleHttp\Client;


class UserController  extends Controller
{
    /**
     * @Route("/user", name="user")
     */
    public function userAction(Request $request)
    {


        $client = new Client(['base_uri' => 'https://jsonplaceholder.typicode.com']);

        $response = $client->request('GET', 'users');

        $users =  $response->getBody()->getContents();

        $manage = json_decode($users);

        return $this->render('default/user.html.twig' , [
            'users' => $manage,
        ]);
    }
}